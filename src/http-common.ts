import axios, { AxiosInstance } from "axios";

const api: AxiosInstance = axios.create({
    baseURL: "https://api.infoip.io",
    headers: {
        "Content-type": "application/json",
    },
});

export default api;