module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '.'
    : '/',
  chainWebpack: config => {
    config.module.rules.delete('eslint');
    config.optimization.delete('splitChunks') // no vendor chunks
    config.plugins.delete('prefetch')         // no prefetch chunks
    config.plugins.delete('preload')          // no preload chunks
  },

  configureWebpack: {
    performance: {
      hints: false
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 200000,
      }
    }
  }
}
